/**
 *
 * @param {string} label - to use on log string
 * @param {string} sublabel - (optional) - to use in log string
 * @param {string} logLevel - (optional) log level
 * @param {Object} sentryOptions - Sentry credentials
 * @return {Logger}
 */
declare function createLogger({label, sublabel, logLevel, sentryOptions}: {label: string, sublabel: string, logLevel?: string, sentryOptions?: any}): Logger;

interface Logger {
    info(...args: any[]): void;
    debug(...args: any[]): void;
    error(...args: any[]): void;
    warn(...args: any[]): void;
}

export = createLogger;
