# devorchestra-logger
A wrapper for npm [winston-logger module](https://www.npmjs.com/package/winston). It sends logs to a process stdout and to the [Sentry]() if credentials are provided(https://sentry.io)

## Configuration
Need to provide the following environment variables:
    
    * NODE_ENV
    * LOG_LEVEL

## How to use
See index.d.ts types file

## Documentation
You can use `typedoc` to generate documentation of the module:
```
$ typedoc --out docs/ --includeDeclarations index.d.ts
```
