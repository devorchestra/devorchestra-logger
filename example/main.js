let buildLogger = require('../index');
let getPreparedLogger = require('./loggerWrapper');

let logger1 = buildLogger({label: "aws-sqs-wrapper", logLevel: process.env.SERVICE_LOG_LEVEL || "info"});

let logger2 = buildLogger({label: "express", sublabel: "users-endpoints", logLevel: process.env.APP_LOG_LEVEL || "debug"});

let logger3 = getPreparedLogger('args-validator');

logger1.info('raz', {a: 1});
logger2.info('dva', {b: 2});
logger3.info('tree', {c: 3});

