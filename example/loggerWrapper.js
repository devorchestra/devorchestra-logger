let buildLogger = require('../index');

module.exports = (sublabel) => {
    return buildLogger({label: "express", sublabel, logLevel: process.env.APP_LOG_LEVEL || "debug"})
}