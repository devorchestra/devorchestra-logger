let util = require('util');
const winston = require('winston');
const Sentry = require('winston-raven-sentry');

const {combine, timestamp, printf} = winston.format;

LOG_LEVELS = ['info', 'error', 'warn', 'debug'];


module.exports = function ({label, sublabel, logLevel, sentryOptions}) {

    if (!logLevel) logLevel = 'info';
    if (!label) throw new Error('`label` field is required for logger initialization');


    const logFormat = printf(info => {
        if (sublabel) {
            return `${info.timestamp} [${label}]:[${sublabel}] [${info.level.toUpperCase()}] -> ${info.message}`
        } else {
            return `${info.timestamp} [${label}] [${info.level.toUpperCase()}] -> ${info.message}`
        }
    });

    let transports = [
        new winston.transports.Console({
            format: combine(
                timestamp(),
                logFormat
            )
        })
    ];

    if (sentryOptions && process.env.NODE_ENV.toLowerCase() != 'development') {
        transports.push(new Sentry(sentryOptions))
    }

    const logger = winston.createLogger({
        level: logLevel,
        transports: transports
    });

    let loggerWrapper = {};
    for (let levelName of LOG_LEVELS) {
        loggerWrapper[levelName] = function (...args) {
            let message = '';
            for (let arg of args) {
                message += util.format(arg) + ' ';
            }
            logger[levelName](message);
        }
    }

    return loggerWrapper
};